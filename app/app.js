'use strict';
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var movieShakeApp = angular.module('MovieShake', [
  'ionic',
  'config',
  'ngCordova',
  'rzModule',
  'debounce',
  'angular-loading-bar',
  'blurredImageDirective',
  'autoHeightHeaderFooterDirective',
  'trustedResourceFilter',
  'movieGenresFilter',
  'movieDirectorFilter',
  'movieCastFilter',
  'movieRevenueFilter',
  'discoverService',
  'movieService',
  'genreService',
  'searchService',
  'movieShakeController',
  'shakerController',
  'noMoviesController',
  'filtersMenuController',
]);

movieShakeApp
  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if(window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('movieshake', {
          url: '',
          abstract: true,
          templateUrl: 'movie_shake/movieshake.html',
          controller: 'MovieShakeCtrl'
        })
        .state('movieshake.shaker', {
          url: '/shaker',
          views: {
            'content': {
              templateUrl: 'components/shaker/shaker.html',
              controller: 'ShakerCtrl'
            },
            'filtersMenu': {
              templateUrl: 'components/filters/filters-menu.html',
              controller: 'FiltersMenuCtrl'
            }
          }
        })
        .state('movieshake.shaker.nomovies', {
          url: '/shaker/nomovies',
          views: {
            'content@movieshake': {
              templateUrl: 'components/shaker/nomovies/nomovies.html',
              controller: 'NoMoviesCtrl'
            }
          }
        });
  
      $urlRouterProvider.otherwise('/shaker');
    }
  ])

  .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
  }]);
