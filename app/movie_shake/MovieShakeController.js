'use strict';

var movieShakeController = angular.module('movieShakeController', []);

movieShakeController.controller('MovieShakeCtrl', ['$scope', '$ionicPopover',
  function($scope, $ionicPopover) {
    $scope.filters = { with_genres: "" };

    // .fromTemplateUrl() method
    $ionicPopover.fromTemplateUrl('movie_shake/about-popover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.aboutPopover = popover;
    });

    $scope.openAboutPopover = function($event) {
      $scope.aboutPopover.show($event);
    };
    $scope.closeAboutPopover = function() {
      $scope.aboutPopover.hide();
    };
  }
]);