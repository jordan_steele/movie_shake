'use strict';

var blurredImageDirective = angular.module('blurredImageDirective', [])

blurredImageDirective.directive('blurredImage', function(){
  return {
      restrict: 'E',
      replace: true,
      scope: {
        src:'@',
        cssClass:'=',
        ctrlFn: '&'
      },
      link: function(scope, element, attrs) {

        scope.itemId = "canvas_" + parseInt(Math.random()*1000000000)
        scope.height = element.parent()[0].offsetHeight;
        scope.width = element.parent()[0].offsetWidth;

        var fillCanvas = function() {
          if(!scope.src || !scope.width)
            return;
          
          var context = element[0].getContext("2d"); // get the 2d context object
          var img     = new Image() //create a new image
         
          img.onload = function(){
            // Adjust image to be centred and to fill screen whilst maintaining aspect ratio
            var canvasAspectRatio = scope.width / scope.height;
            var drawnImageAspectRatio = img.width / img.height;
            var drawnImageWidth = img.width;
            var drawnImageHeight = img.height;

            if (drawnImageWidth < scope.width) {
              drawnImageWidth = scope.width;
              drawnImageHeight = drawnImageWidth / drawnImageAspectRatio;
            }
            if (drawnImageHeight < scope.height) {
              drawnImageHeight = scope.height;
              drawnImageWidth = drawnImageHeight * drawnImageAspectRatio;
            }
            if (drawnImageHeight > scope.height && drawnImageWidth > scope.width) {
              if (canvasAspectRatio > drawnImageAspectRatio) {
                drawnImageWidth = scope.width;
                drawnImageHeight = drawnImageWidth / drawnImageAspectRatio;
              }
              if (canvasAspectRatio <= drawnImageAspectRatio) {
                drawnImageHeight = scope.height;
                drawnImageWidth = drawnImageHeight * drawnImageAspectRatio;
              }
            }

            // Draw Image
            context.drawImage(img, (scope.width - drawnImageWidth) / 2,
                                   (scope.height - drawnImageHeight) / 2,
                                   drawnImageWidth,
                                   drawnImageHeight); // draw the image at the given location

            // Blur Image
            boxBlurCanvasRGBA( scope.itemId, 0, 0, scope.width, scope.height, 10, 2);

            // Apply a transparent overlay to help readability
            context.beginPath();
            context.rect(0, 0, scope.width, scope.height);
            context.fillStyle = "rgba(0, 0, 0, 0.4)";
            context.fill();

            // Tell controller that the image has loaded
            scope.ctrlFn();
          };
          
          img.crossOrigin = '';
          img.src = scope.src; 
        }

        scope.$watch('src', function(){
          fillCanvas();
        });
      },
      template: '<canvas id="{{ itemId }}" class="{{ cssClass }}" width="{{width}}" height="{{height}}" style="position: absolute; z-index: -1;"></canvas>'
  };
});
