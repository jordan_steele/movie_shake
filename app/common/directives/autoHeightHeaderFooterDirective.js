'use strict';

var autoHeightHeaderFooterDirective = angular.module('autoHeightHeaderFooterDirective', [])

autoHeightHeaderFooterDirective.directive('autoHeightHeaderFooter', function(){
  return {
      restrict: 'E',
      transclude: true,

      link: function(scope, element, attrs, $transclude) {
        var header = element.find('ion-header-bar')[0];
        var content = element.find('ion-content');
        var footer = element.find('ion-footer-bar')[0];

        scope.$watch(
          function() {
            return header.clientHeight;
          },
          function(newHeaderHeight, oldHeaderHeight) {
            content.css({
              top: newHeaderHeight + "px"
            });
          });

        scope.$watch(
          function() {
            return footer.clientHeight;
          },
          function(newFooterHeight, oldFooterHeight) {
            content.css({
              bottom: newFooterHeight + "px"
            });
          });
      },
      template: '<div ng-transclude></div>'
  };
});
