'use strict';

var searchService = angular.module('searchService', ['ngResource']);

searchService.factory('Search', ['$resource', 'MOVIEDB',
  function($resource, MOVIEDB){
    return $resource(MOVIEDB.host + MOVIEDB.searchEndpoint + '/:type' + "?api_key=" + MOVIEDB.apiKey, {}, {
      'query': { method: "GET", isArray: false }
    });
  }]);