'use strict';

var trustedResourceFilter = angular.module('trustedResourceFilter', []);

trustedResourceFilter.filter('trusted', ['$sce', function ($sce) {
  return function(url) {
    return $sce.trustAsResourceUrl(url);
  };
}]);