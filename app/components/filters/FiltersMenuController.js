'use strict';

var filtersMenuController = angular.module('filtersMenuController', []);

filtersMenuController.controller('FiltersMenuCtrl', ['$scope', '$ionicSideMenuDelegate', '$ionicModal', 'debounce', 'Genre', 'Search',
  function($scope, $ionicSideMenuDelegate, $ionicModal, debounce, Genre, Search) {

    /*
     * Functions for updating the filters object on the parent scope based on inputs
     */

    $scope.updatePeopleFilters = function() {
      var peopleFilterString = "";
      angular.forEach($scope.people.selected, function(person, index) {
        peopleFilterString += person.id + ",";
      });
      $scope.filters.with_people = peopleFilterString;
    };

    $scope.updateAverageVoteFilters = function() {
      $scope.filters['vote_average.gte'] = $scope.averageVoteSlider.min;
      $scope.filters['vote_average.lte'] = $scope.averageVoteSlider.max;
      if ($scope.averageVoteSlider.min != 0 || $scope.averageVoteSlider != 0) {
        $scope.filters['vote_count.gte'] = 50;
      }
    };

    $scope.updateReleaseYearFilters = function() {
      $scope.filters['primary_release_date.gte'] = $scope.releaseYearSlider.min + "-01-01";
      $scope.filters['primary_release_date.lte'] = $scope.releaseYearSlider.max + "-12-31";
    };

    $scope.updateGenreFilters = function() {
      var genreFilterString = "";
      angular.forEach($scope.genres.genres, function(genre, index) {
        if (genre.checked == true) {
          genreFilterString += genre.id + ",";
        }
      });
      $scope.filters.with_genres = genreFilterString;
    };

    /*
     * Creation of modals and functions for opening/closing/destroying
    */

    $ionicModal.fromTemplateUrl('components/filters/people/people-filter.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.peopleModal = modal
    });

    $scope.openPeopleModal = function() {
      $scope.peopleModal.show();
    };

    $scope.closePeopleModal = function() {
      $scope.peopleModal.hide();
    };

    $ionicModal.fromTemplateUrl('components/filters/genre/genre-filter.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.genreModal = modal
    });

    $scope.openGenreModal = function() {
      $scope.genreModal.show();
    };

    $scope.closeGenreModal = function() {
      $scope.genreModal.hide();
      $scope.updateGenreFilters();
    };

    $scope.$on('$destroy', function() {
      $scope.genreModal.remove();
      $scope.peopleModal.hide();
    });

    $scope.searchPeople = function(query) {
      if (query != '' && query != null) {
        $scope.people.results = Search.query({type: 'person', query: query});
      } else {
        $scope.people.results = null;
      }
    };

    $scope.selectPerson = function(person) {
      if ($scope.people.selected.indexOf(person) <= -1) {
        $scope.people.selected.push(person);
      }
      $scope.updatePeopleFilters();
      $scope.closePeopleModal();
    };

    $scope.removePerson = function(person) {
      $scope.people.selected.splice( $scope.people.selected.indexOf(person), 1 );
      $scope.updatePeopleFilters();
    };

    $scope.removeGenre = function(genre) {
      angular.forEach($scope.genres.genres, function(genre_, index) {
        if (genre == genre_) {
          genre_.checked = false;
        }
      });
      $scope.updateGenreFilters();
    };

    $scope.genres = Genre.query(function(genres) {
      angular.forEach(genres.genres, function(genre, index) {
        genre.checked = false;
      });
    });

    $scope.averageVoteSlider = {
      min: 0,
      max: 10,
      ceil: 10,
      floor: 0
    };

    var currentYear = new Date().getFullYear();
    $scope.releaseYearSlider = {
      min: 1880,
      max: currentYear,
      ceil: currentYear,
      floor: 1880
    };

    $scope.$watch(
      function() {
        return $scope.averageVoteSlider.min;
      },
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.updateAverageVoteFilters();
        }
      }
    );
    $scope.$watch(
      function() {
        return $scope.averageVoteSlider.max;
      },
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.updateAverageVoteFilters();
        }
      }
    );

    $scope.$watch(
      function() {
        return $scope.releaseYearSlider.min;
      },
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.updateReleaseYearFilters();
        }
      }
    );
    $scope.$watch(
      function() {
        return $scope.releaseYearSlider.max;
      },
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.updateReleaseYearFilters();
        }
      }
    );

    // $scope.peopleSearchResults = {};
    $scope.people = { selected: [], results: null };
    var debouncedSearchPeople = debounce($scope.searchPeople, 1000);
    $scope.$watch(
      function() {
        return $scope.people.query;
      },
      function(newQuery, oldQuery) {
        debouncedSearchPeople(newQuery);
      })

  }
]);