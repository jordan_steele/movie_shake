'use strict';

var genreService = angular.module('genreService', ['ngResource']);

genreService.factory('Genre', ['$resource', 'MOVIEDB',
  function($resource, MOVIEDB){
    return $resource(MOVIEDB.host + MOVIEDB.genreEndpoint + "?api_key=" + MOVIEDB.apiKey, {}, {
      'query': { method: "GET", isArray: false }
    });
  }]);