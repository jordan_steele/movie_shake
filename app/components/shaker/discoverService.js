'use strict';

var discoverService = angular.module('discoverService', ['ngResource']);

discoverService.factory('Discover', ['$resource', 'MOVIEDB',
  function($resource, MOVIEDB){
    return $resource(MOVIEDB.host + MOVIEDB.discoverEndpoint + "?api_key=" + MOVIEDB.apiKey, {}, {
      'query': { method: "GET", isArray: false }
    });
  }]);