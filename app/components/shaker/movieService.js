'use strict';

var movieService = angular.module('movieService', ['ngResource']);

movieService.factory('Movie', ['$resource', 'MOVIEDB',
  function($resource, MOVIEDB){
    return $resource(MOVIEDB.host + MOVIEDB.movieEndpoint + '/:id:sub?api_key=' + MOVIEDB.apiKey , {sub: '@sub'}, {
    });
  }]);