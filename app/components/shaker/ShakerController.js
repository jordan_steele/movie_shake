'use strict';

var shakerController = angular.module('shakerController', []);

shakerController.controller('ShakerCtrl', ['$scope', '$state', '$q', '$timeout', '$ionicPlatform', '$cordovaDeviceMotion', 'debounce', 'Discover', 'Movie',
  function($scope, $state, $q, $timeout, $ionicPlatform, $cordovaDeviceMotion, debounce, Discover, Movie) {

    $scope.getMovieData = function(id, sub) {
      var d = $q.defer();
      var result = Movie.get({id: id, sub: sub}, function() {
        d.resolve(result);
      });
      return d.promise;
    };

    $scope.getMovieDetail = function(id) {
      $q.all([
        $scope.getMovieData(id, ''),
        $scope.getMovieData(id, '/credits')
      ]).then(function(data) {
        // Start loading in the new background and save movie data for when it's loaded
        nextMovieLoading = data[0];
        $scope.currentMovie.backgroundImagePath = "http://image.tmdb.org/t/p/w92" + data[0].poster_path;
        nextMovieLoading.credits = data[1];
      });
    };

    /*
      Update the current movie to the newly loaded movie
    */
    $scope.updateCurrentMovie = function() {
      nextMovieLoading.backgroundImagePath = $scope.currentMovie.backgroundImagePath;
      $scope.currentMovie = nextMovieLoading;
    };

    $scope.nextPage = function(filters) {
      var params = angular.copy(filters);

      var pageIndex = Math.floor(Math.random() * pagesNotSeen.length);
      pagesNotSeen.splice(pageIndex, 1);
      params.page = pagesNotSeen[pageIndex];
      movies = Discover.query(params, function(movies) {
        $scope.nextMovie();
      });
    };

    $scope.updateMovies = function(filters) {
      var params = angular.copy(filters);

      params.page = 1;
      Discover.query(params, function(movies_) {
        if (!movies_.total_results) {
          $state.go('movieshake.shaker.nomovies');
        } else {
          pagesNotSeen = [];
          for (var i=0, len=Math.min(1000, movies_.total_pages); i < len; i++) {
            pagesNotSeen[i] = i+1;
          }
          $scope.nextPage(filters);
        }
      });
    };

    $scope.nextMovie = function() {
      if (movies.results.length === 0) {
        if (pagesNotSeen.length === 0) {
          $state.go('movieshake.shaker.nomovies');
        } else {
          $scope.nextPage($scope.filters);
        }
      } else {
        var nextMovieIndex = Math.floor(Math.random() * movies.results.length);
        var nextMovie = movies.results[nextMovieIndex];
        movies.results.splice(nextMovieIndex, 1);
        if (nextMovie.poster_path) {
          $scope.getMovieDetail(nextMovie.id);
        } else {
          $scope.nextMovie();
        }
      }
    }

    $scope.startWatch = function(options) {
      var previousAcceleration = { x: null, y: null, z: null };
      var watch = $cordovaDeviceMotion.watchAcceleration(options);
      watch.then(
        null,
        function(error) {
        // An error occurred
        },
        function(acceleration) {
          var accelerationChange = 0;

          if (previousAcceleration.x != null) {
            accelerationChange += Math.abs(previousAcceleration.x, acceleration.x);
            accelerationChange += Math.abs(previousAcceleration.y, acceleration.y);
            accelerationChange += Math.abs(previousAcceleration.z, acceleration.z);
          }

          if (accelerationChange > 22) {
            previousAcceleration = {
              x: null,
              y: null,
              z: null
            }
            $scope.nextMovie();
            watch.clearWatch();
            $timeout(function(){ $scope.startWatch(options); }, 1000);
          } else {
            previousAcceleration = {
              x: acceleration.x,
              y: acceleration.y,
              z: acceleration.z
            }
          }
      });
    }

    var movies = {};
    var pagesNotSeen = [];
    var nextMovieLoading = {};
    $scope.currentMovie = {};

    $scope.updateMovies($scope.filters);

    var debouncedUpdateMovies = debounce($scope.updateMovies, 500);
    $scope.$watch('filters', function(newFilters, oldFilters) {
      if (newFilters != oldFilters) {
        debouncedUpdateMovies(newFilters);
      }
    }, true);

    var options = {
      frequency: 200
    }

    $ionicPlatform.ready(function() {
      $scope.startWatch(options);
    });

  }]);
