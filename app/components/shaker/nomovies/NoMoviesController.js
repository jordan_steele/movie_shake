'use strict';

var noMoviesController = angular.module('noMoviesController', []);

noMoviesController.controller('NoMoviesCtrl', ['$scope', '$state',
  function($scope, $state) {
    $scope.$watch('filters', function(newFilters, oldFilters) {
      console.log(newFilters);
      if (newFilters != oldFilters) {
        $state.go('movieshake.shaker');
      }
    }, true);
  }]);
