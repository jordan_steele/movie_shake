'use strict';

var movieGenresFilter = angular.module('movieGenresFilter', []);

movieGenresFilter.filter('movieGenres', function () {
  return function(movie) {
    if (!movie || !movie.genres) 
      return;
    return movie.genres.map(function(genre) {
      return genre.name;
    }).join(", ");
  };
});