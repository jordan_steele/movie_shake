'use strict';

var movieRevenueFilter = angular.module('movieRevenueFilter', []);

movieRevenueFilter.filter('movieRevenue', function () {
  return function(movie) {
    if (!movie) {
      return;
    } else if (!movie.revenue) {
      return "n/a";
    } else if (movie.revenue < 1000000000) {
      return Math.round(movie.revenue / 1000000) + "m";
    } else {
      return Math.round(movie.revenue / 1000000000 * 100) / 100 + "b";
    }
  };
});