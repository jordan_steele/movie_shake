'use strict';

var movieDirectorFilter = angular.module('movieDirectorFilter', []);

movieDirectorFilter.filter('movieDirector', function () {
  return function(movie) {
    if (!movie || !movie.credits || !movie.credits.crew) 
      return;

    for (var i = 0; i < movie.credits.crew.length; i++) {
      if (movie.credits.crew[i].job == "Director") {
        return movie.credits.crew[i].name;
      }
    }
  };
});