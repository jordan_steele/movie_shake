'use strict';

var movieCastFilter = angular.module('movieCastFilter', []);

movieCastFilter.filter('movieCast', function () {
  return function(movie) {
    if (!movie || !movie.credits || !movie.credits.cast) {
      return;
    }

    var includedCast = []
    for (var i = 0; i < 5 && i < movie.credits.cast.length; i++) {
      includedCast.push(movie.credits.cast[i].name);
    }
    return includedCast.join(", ");
  };
});