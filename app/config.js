"use strict";

 angular.module("config", [])

.constant("ENV", {
  "name": "development",
  "apiEndpoint": "http://dev.yoursite.com:10000/"
})

.constant("MOVIEDB", {
  "host": "http://api.themoviedb.org/3/",
  "discoverEndpoint": "discover/movie",
  "genreEndpoint": "genre/movie/list",
  "searchEndpoint": "search",
  "movieEndpoint": "movie",
  "apiKey": "6d3711f1b9e7d4f148c54df19b81daae"
})

;